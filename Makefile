#
#	Makefile for urlmetrics
#

OBJS	= lex.yy.o config.o metrics.o processor.o
CC	= gcc
PCRE2_INCS = -Ipcre2-10.20/src
PCRE2_LIBS = ../pcre2-10.20/.libs/libpcre2-8.a ../pcre2-10.20/.libs/libpcre2-posix.a
PCRE2_LIBS =
CFLAGS	= -ggdb -Wall $(PCRE2_INCS)


default: all

all: urlmetrics

config.tab.c: config.y urlmetrics.h
	bison -d config.y

lex.yy.c: config.l
	flex config.l
	
lex.yy.o: lex.yy.c
	$(CC) $(CFLAGS) -o lex.yy.o -c lex.yy.c

config.o: config.tab.c config.h
	$(CC) $(CFLAGS) -o config.o -c config.tab.c

metrics.o: metrics.c metrics.h
	$(CC) $(CFLAGS) -o metrics.o -c metrics.c

processor.o: processor.c processor.h
	$(CC) $(CFLAGS) -o processor.o -c processor.c

urlmetrics: urlmetrics.c metrics.o processor.o config.o lex.yy.o urlmetrics.h
	$(CC) $(CFLAGS) -o urlmetrics urlmetrics.c metrics.o processor.o config.o lex.yy.o \
		$(PCRE2_LIBS) \
		-lnids -L/usr/local/opt/flex/lib -lfl -ly

clean:
	rm -f lex.yy.c config.tab.c config.tab.h $(OBJS)
