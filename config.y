%{
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "urlmetrics.h"
#include "metrics.h"

// stuff from flex that bison needs to know about:
extern int yylex();
extern int yyparse();
extern FILE *yyin;

void yyerror(const char *s);

char *interface = NULL;

int input_section_found = 0; 
int graphite_section_found = 0; 
int metrics_configured = 0;

struct metric_struct *latest_metric = NULL;

char *metric_name = NULL, *method = NULL, *url = NULL;
int interval = -1;

char *pattern = NULL;

int num_request_header_patterns = 0;
struct header_pattern **request_header_patterns = NULL;
int num_response_header_patterns = 0;
struct header_pattern **response_header_patterns = NULL;

%}

%error-verbose
%{
#define YYDEBUG 1
%}

%union {
	int ival;
	float fval;
	char *sval;
}


// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <ival> INTEGER
%token <fval> FLOAT
%token <sval> STRING

%token Input Interface
%token Graphite Server Port
%token Metric_Prefix LogLevel
%token Metric Method URL Interval Request_Header Response_Header
%token START_OF_BLOCK END_OF_BLOCK SEMICOLON

%define "parse.error" "verbose"
%define "parse.lac" "full"

%%

// this is the actual grammar that bison will parse, but for right now it's just
// something silly to echo to the screen what bison gets from flex.  We'll
// make a real one shortly:
sn_configuration:
	statements;

statements:
	statement SEMICOLON statements { }
	| statement
	;


INPUT_SECTION: START_OF_BLOCK input_section_statements END_OF_BLOCK { };

input_section_statement:
	Interface STRING       { interface = $2; }
	|;

	
input_section_statements:
	input_section_statement SEMICOLON input_section_statements { }
	| input_section_statement
	;


GRAPHITE_SECTION: START_OF_BLOCK graphite_section_statements END_OF_BLOCK { };


graphite_section_statement:
	Server STRING       { graphite_server = $2; }
	| Port INTEGER	      { graphite_port = $2; }
	|;

	
graphite_section_statements:
	graphite_section_statement SEMICOLON graphite_section_statements { }
	| graphite_section_statement
	;


METRIC_SECTION: START_OF_BLOCK metric_section_statements END_OF_BLOCK { }

metric_section_statement:
        Method STRING		{ method = $2; }
        | URL STRING		{ url = $2; }
        | Interval INTEGER	{ interval = $2; }
        | Request_Header  STRING STRING { ++num_request_header_patterns;
                                          if(!request_header_patterns) request_header_patterns = malloc( sizeof(struct header_pattern *) );
                                          else request_header_patterns = realloc( &request_header_patterns, sizeof(struct header_pattern *) * num_request_header_patterns);
                                          
                                          request_header_patterns[num_request_header_patterns-1] = malloc( sizeof(struct header_pattern) );
                                          request_header_patterns[num_request_header_patterns-1]->header_name = $2;
                                          request_header_patterns[num_request_header_patterns-1]->header_value_regex = $3;
                                          asprintf(&pattern, "\n%s: %s\r\n", $2, $3);
                                          
                                          if( regcomp(&request_header_patterns[num_request_header_patterns-1]->header_value_preg, pattern, REG_EXTENDED|REG_ENHANCED) != 0 ) {
                                                fprintf(stderr, "Header regex \"%s\" metric (derived from header \"%s\" and pattern \"%s\") failed compilation!\n",
                                                        pattern, $2, $3);
                                                exit(1);
                                          }
                                          free(pattern);
                                        }
        | Response_Header STRING STRING {
                                          ++num_response_header_patterns;
                                          if(!response_header_patterns) response_header_patterns = malloc( sizeof(struct header_pattern *) );
                                          else response_header_patterns = realloc( &response_header_patterns, sizeof(struct header_pattern *) * num_response_header_patterns);
                                          
                                          response_header_patterns[num_response_header_patterns-1] = malloc( sizeof(struct header_pattern) );
                                          response_header_patterns[num_response_header_patterns-1]->header_name = $2;
                                          response_header_patterns[num_response_header_patterns-1]->header_value_regex = $3;
                                          asprintf(&pattern, "\n%s: %s\r\n", $2, $3);
                                          
                                          if( regcomp(&response_header_patterns[num_response_header_patterns-1]->header_value_preg, pattern, REG_EXTENDED|REG_ENHANCED) != 0 ) {
                                                fprintf(stderr, "Header regex \"%s\" metric (derived from header \"%s\" and pattern \"%s\") failed compilation!\n",
                                                        pattern, $2, $3);
                                                exit(1);
                                          }
                                          free(pattern);
                                        }
        |;

metric_section_statements:
	metric_section_statement SEMICOLON metric_section_statements { }
	| metric_section_statement
	;
	

statement:
	| Input INPUT_SECTION			{ input_section_found = 1; }
	| Graphite GRAPHITE_SECTION		{ graphite_section_found = 1; }
	| Metric_Prefix STRING			{ metric_prefix = $2; }
	| LogLevel INTEGER			{ debug = $2; }
	| Metric STRING METRIC_SECTION		{ ++metrics_configured; metric_name = $2;
                                                  latest_metric = new_metric(metric_name, interval, method, url);
                                                  
                                                  latest_metric->num_request_header_patterns = num_request_header_patterns;
                                                  latest_metric->request_header_patterns = request_header_patterns;
                                                  latest_metric->num_response_header_patterns = num_response_header_patterns;
                                                  latest_metric->response_header_patterns = response_header_patterns;
                                                  
                                                  metric_name = NULL;
                                                  interval = -1;
                                                  method = NULL;
                                                  url = NULL;
                                                  num_request_header_patterns = 0;
                                                  num_response_header_patterns = 0;
                                                  request_header_patterns = NULL;
                                                  response_header_patterns = NULL;
                                                  latest_metric = NULL;
                                                }
	;
%%

extern int linenum, colnum;

void load_config(char *filename) {
        FILE *f;

        if( (f=fopen(filename,"r")) ) {
                yyin = f;
                
                //yylex();
                // parse through the input until there is no more:
                do {
                        yyparse();
                        
                        if(!input_section_found) {
                        	fprintf(stderr,"No \"input\" section found in configuration file.\n");
                        	exit(1);
                        }
                        if(!graphite_section_found) {
                        	fprintf(stderr,"No \"graphite\" section found in configuration file.\n");
                        	exit(1);
                        }
                        if(metrics_configured == 0) {
                        	fprintf(stderr,"No metrics configured in configuration file. Specify at least one.\n");
                        	exit(1);
                        }
                } while (!feof(yyin));
                
                /* printf("%d metrics configured.\n", metrics_configured); */
                
                fclose(f);
        } else {
                fprintf(stderr,"Unable to open config file `%s'.\n", filename);
                exit(1);
        }
}

void yyerror(const char *s) {
	printf("Configuration file parse error at line %d, column %d: `%s'\n",
	        linenum, colnum, s);
	exit(1);
}
