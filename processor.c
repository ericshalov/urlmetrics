/*
  processor.c
  ems dec2015
  
  TODO:
  Support for monitoring proxied connections
*/

#include <nids.h>
#include <regex.h>
#include <ctype.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>

#include "urlmetrics.h"
#include "metrics.h"

struct http_transaction {
    struct tuple4 addr;
    int connection_index;

    /* Request */
    struct timeval request_start;
    char *request_header;
    char *method, *url;
    long content_length;

    /* Response */
    struct timeval first_byte, last_byte;
    char *response_message;
    char *response_header;
    int response_code;
};

regex_t request_preg, host_preg;
regex_t response_preg, content_length_preg;

/* counters */
unsigned long total_requests = 0, total_responses = 0;
unsigned long active_sessions = 0, active_requests = 0;

#define REQUEST_BUF_SIZE  1024*128
#define RESPONSE_BUF_SIZE 1024*128

struct http_session {
    struct timeval start;
    struct timeval close;
    
    struct http_transaction **requests;
    int num_requests;
    
    int num_responses;

    char request_buf[REQUEST_BUF_SIZE];
    char response_buf[RESPONSE_BUF_SIZE];
    int in_content;
    long content_length;
    unsigned long content_received;
    unsigned long request_bytes;
    unsigned long response_bytes;
    struct timeval elapsed;
};

/* prototypes */
int timeval_subtract (struct timeval *result,struct timeval *x,struct timeval *y);
void handle_data_from_client(struct tcp_stream *ts);
void handle_data_from_server(struct tcp_stream *ts);
void process_http_transaction(struct http_transaction *http);


void init_tcp_processor() {
    /* Compile regexes */
    if( regcomp(&request_preg, "^(GET|HEAD|PUT|DELETE|OPTIONS|TRACE|CONNECT|POST) (.*?) HTTP/(1.1)(.*?)\r\n\r\n", REG_EXTENDED|REG_ENHANCED) != 0 ) {
        fprintf(stderr,"Request regex failed compilation!\n");
        exit(2);
    }
    if( regcomp(&response_preg, "^HTTP/1.1 ([0-9]+) (.+?)\r\n(.*?)\r\n\r\n", REG_EXTENDED|REG_ENHANCED) != 0 ) {
      fprintf(stderr,"Response regex failed compilation!\n");
      exit(2);
    }
    if( regcomp(&host_preg, "\r\nHost: (.*?)\r\n", REG_EXTENDED|REG_ENHANCED) != 0 ) {
      fprintf(stderr,"Host regex failed compilation!\n");
      exit(2);
    }
    if( regcomp(&content_length_preg, "\r\nContent-Length: ([0-9]+)\r\n", REG_EXTENDED|REG_ENHANCED) != 0 ) {
      fprintf(stderr,"Content-Length regex failed compilation!\n");
      exit(2);
    }
}    


void *tcp_processor(struct tcp_stream *ts, void **param) {
    struct http_session *hs = NULL;
    int bytes_to_save = -1;
    int i;
    hs = (struct http_session *)ts->user;
    
    if(debug >= 3) debuglog("tcp_processor(ts = {dest=%d, client.count_new = %d, server.count_new = %d}, param)", ts->addr.dest, ts->client.count_new, ts->server.count_new);
    
    if(ts->addr.dest == 80) {
        if(debug >= 2) {
            debuglog("TCP %s %d => %d ",
              ts->nids_state == NIDS_JUST_EST ? "NEW":
              ts->nids_state == NIDS_DATA ? "DATA":
              ts->nids_state == NIDS_CLOSE ? "CLS":
              ts->nids_state == NIDS_RESET ? "RST":
              ts->nids_state == NIDS_TIMED_OUT ? "TIM":
              "???",
              ts->addr.source, ts->addr.dest);
            if(ts->client.count_new) printf("(SERVER sent => %d bytes. response_bytes = %lu) ", ts->client.count_new, hs->response_bytes);
            if(ts->server.count_new) printf("(CLIENT sent => %d bytes. request_bytes = %lu) ", ts->server.count_new, hs->request_bytes);
        }
            
        if(ts->nids_state == NIDS_JUST_EST) {
            ++active_sessions;
            hs = malloc(sizeof(struct http_session));
            gettimeofday(&hs->start, NULL);
            hs->requests = NULL;
            hs->num_requests = 0;
            hs->num_responses  = 0;
            hs->request_bytes = 0;
            hs->response_bytes = 0;
            hs->elapsed.tv_sec = 0; hs->elapsed.tv_usec = 0;
            hs->in_content = 0;
            hs->content_received = 0;
            ts->user = hs;
        }
        
        if(ts->nids_state == NIDS_CLOSE) {
           if(ts->user) {
               gettimeofday(&hs->close, NULL);
               timeval_subtract(&hs->elapsed, &hs->close, &hs->start);
               if(debug >= 2) debuglog("HTTP SESSION (%d => %d) elapsed = %ld.%06d secs",
                   ts->addr.source, ts->addr.dest,
                   hs->elapsed.tv_sec, hs->elapsed.tv_usec);
               for(i=0 ; i < hs->num_requests ; i++) {
                 free(hs->requests[i]->response_message);
                 free(hs->requests[i]->request_header);
                 free(hs->requests[i]->response_header);
                 free(hs->requests[i]->method);
                 free(hs->requests[i]->url);
                 free(hs->requests[i]); /* free the struct */
               }
               free(hs->requests); /* free the array of pointers */

               free(ts->user);
           }
           hs->request_bytes = 0;
           hs->response_bytes = 0;
           --active_sessions;
        }
          
        if(ts->nids_state == NIDS_RESET) {
            hs->request_bytes = 0;
            hs->response_bytes = 0;
            --active_sessions;
        }

        if(ts->nids_state == NIDS_TIMED_OUT) {
            hs->request_bytes = 0;
            hs->response_bytes = 0;
            --active_sessions;
        }

        ts->client.collect = 1;
        ts->server.collect = 1;

        if(ts->server.count_new) {
            /* handle new data that just arrived */
            if(hs->request_bytes < REQUEST_BUF_SIZE) {
              bytes_to_save = (hs->request_bytes+ts->server.count_new) <= REQUEST_BUF_SIZE ? ts->server.count_new : (REQUEST_BUF_SIZE-hs->request_bytes);
              memcpy(hs->request_buf+hs->request_bytes, ts->server.data,  bytes_to_save);
              hs->request_bytes += bytes_to_save;
              if(debug >= 3) debuglog("{ts->server.count_new = %d, request_bytes = %lu, REQUEST_BUF_SIZE = %d, bytes_to_save = %d}", ts->server.count_new, hs->request_bytes, REQUEST_BUF_SIZE, bytes_to_save);
            }

            handle_data_from_client(ts);
        }

        // HTTP response:
        if(ts->client.count_new) {
            if(hs->response_bytes < RESPONSE_BUF_SIZE) {
              bytes_to_save = (hs->response_bytes+ts->client.count_new) <= RESPONSE_BUF_SIZE ? ts->client.count_new : (RESPONSE_BUF_SIZE-hs->response_bytes);
              memcpy(hs->response_buf+hs->response_bytes, ts->client.data,  bytes_to_save);
              hs->response_bytes += bytes_to_save;
              if(debug >= 3) debuglog("{ts->client.count_new = %d, response_bytes = %lu, RESPONSE_BUF_SIZE = %d, bytes_to_save = %d}", ts->client.count_new, hs->request_bytes, RESPONSE_BUF_SIZE, bytes_to_save);
            }

            handle_data_from_server(ts);
        }
    } /* if(ts->addr.dest == 80) */

    return NULL;
}


int timeval_subtract (struct timeval *result,struct timeval *x,struct timeval *y) {
  if(debug >= 3) debuglog("timeval_subtract()");
  assert(result);
  assert(x);
  assert(y);

  /* Perform the carry for the later subtraction by updating y. */
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (x->tv_usec - y->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}

/* sometimes two requests arrive in one packet, so we need to shift the first request out of the buffer,
   leaving any remaining requests:

   *FIX* this is only skipping the header, it should really skip all the data too.
   to do this for persistent connections, we need to read the Content-Length: header.
   otherwise, we could theoretically run into a document that has what looks like an HTTP header as it's content,
   and that would make a mess of our packet accounting if the regex matched it as a new request/response.
   We'll also have trouble with a finite REQUEST_BUF_SIZE/RESPONSE_BUF_SIZE, since we could throw away the bulk
   of a packet's content, including the next persistent-HTTP header.
   
   ALSO: If the method == "POST", watch for and skip over POST data, and distinguish it from another request.
*/
void handle_data_from_client(struct tcp_stream *ts) {
      char *request_header = NULL;
      char *method = NULL, *path = NULL, *version = NULL;
      regmatch_t request_pmatch[4+1], host_pmatch[1+1];
      char *host = NULL;
      char url[8000+1];
      char *request_string = NULL;

      struct http_transaction *new_transaction = NULL;
      int keep_scanning = -1;
      
      struct http_session *hs = ts->user;
      
      struct timeval data_arrival_time;

      /* loop through the request_buf, processing an HTTP request, then shifting over the string and looking for the next one */
      keep_scanning = 1;
      while(keep_scanning) {
        request_string = strndup(hs->request_buf, hs->request_bytes);
        gettimeofday(&data_arrival_time, NULL);

        //if(debug >= 3) {
        //  printf("request_string = ["); for(s=request_string;*s;s++) printf("%c", isprint(*s) || *s == '\n' ? *s: '_'); printf("]\n");
        //}

        if( regexec(&request_preg, request_string, 4+1, request_pmatch, 0) == 0 ) {
            if(debug >= 3) debuglog("{request regex matched starting at byte %lld, length %lld bytes.}", request_pmatch[0].rm_so, request_pmatch[0].rm_eo);
            method =         strndup(request_string+request_pmatch[1].rm_so, request_pmatch[1].rm_eo-request_pmatch[1].rm_so);
            path =           strndup(request_string+request_pmatch[2].rm_so, request_pmatch[2].rm_eo-request_pmatch[2].rm_so);
            version =        strndup(request_string+request_pmatch[3].rm_so, request_pmatch[3].rm_eo-request_pmatch[3].rm_so);
            request_header = strndup(request_string+request_pmatch[4].rm_so, request_pmatch[4].rm_eo-request_pmatch[4].rm_so);
            
            ++total_requests;
            
            if( regexec(&host_preg, request_header, 1+1, host_pmatch, 0) == 0 ) {
                host = strndup(request_header+host_pmatch[1].rm_so, host_pmatch[1].rm_eo-host_pmatch[1].rm_so);
            } else {
                /* "Host" header not found, use destination IP in URL: */
                asprintf(&host, "%u.%u.%u.%u", ts->addr.daddr & 0xFF, ts->addr.daddr >> 8 & 0xFF, ts->addr.daddr >>16 & 0xFF, ts->addr.daddr>>24);
            }

            sprintf(url, "http://%s:%d%s", host, ts->addr.dest, path);

            /* allocate new "struct http_transaction" to track request/response */
            ++active_requests;
            new_transaction = malloc(sizeof(struct http_transaction));
            gettimeofday(&new_transaction->request_start, NULL);
            new_transaction->method = strdup(method);
            new_transaction->url    = strdup(url);
            new_transaction->request_header = strdup(request_header);
            new_transaction->response_message = NULL;
            new_transaction->response_header = NULL;
            new_transaction->response_code = -1;
            new_transaction->content_length = -1;
            new_transaction->first_byte.tv_sec = 0; new_transaction->first_byte.tv_usec = 0;
            new_transaction->last_byte.tv_sec = 0;  new_transaction->last_byte.tv_usec = 0;
            new_transaction->connection_index = hs->num_requests;
            memcpy(&new_transaction->request_start, &data_arrival_time, sizeof(struct timeval));
            memcpy(&new_transaction->addr, &ts->addr, sizeof(struct tuple4));
            
            /* resize the hs->requests array of pointers to fit one more pointer to a struct http_transaction */
            ++hs->num_requests;
            if(!hs->requests) hs->requests = malloc(sizeof(struct http_transaction *));
            else hs->requests = realloc(hs->requests,sizeof(struct http_transaction *) * hs->num_requests);
            hs->requests[hs->num_requests-1] = new_transaction;

            if(debug >= 1) debuglog("\nHTTP REQUEST (%lu total processed) (%d => %d #%d)\n  METHOD: %s\n  URL: %s",
              total_requests, ts->addr.source, ts->addr.dest, hs->num_requests, method, url);
            if(debug >= 2) debuglog("  HEADER: \"%s\"", request_header);
            
            free(method);
            free(path);
            free(version);
            free(request_header);
            free(host);

            if(debug >= 3) debuglog("{hs->request_bytes = %lu, request_pmatch[0].rm_eo = %lld}",hs->request_bytes,request_pmatch[0].rm_eo);
            assert(hs->request_bytes >= request_pmatch[0].rm_eo);

            hs->request_bytes -= request_pmatch[0].rm_eo;
            if(hs->request_bytes > 0) {
               memmove(hs->request_buf, hs->request_buf + request_pmatch[0].rm_eo, hs->request_bytes);
               if(debug >= 2) debuglog("{regex match length = %lld, remaining request_bytes = %lu}",
                 request_pmatch[0].rm_eo, hs->request_bytes);
            } else keep_scanning = 0; /* no data left */
        } else keep_scanning = 0; /* didn't find a http request */

        free(request_string);

      } /* while(keep_scanning) */
}

void handle_data_from_server(struct tcp_stream *ts) {
        int keep_scanning = -1;
        regmatch_t response_pmatch[3+1], content_length_pmatch[1+1];
        char *content_length_string = NULL;
        struct http_session *hs = ts->user;
        char *response_string = NULL;
        char *response_code_string = NULL;
        
        struct http_transaction *hr = NULL;
        
        if(debug >= 3) debuglog("handle_data_from_server() {");
        
        keep_scanning = 1;
        while(keep_scanning) {
            if(!hs->in_content) {
                  response_string = strndup(hs->response_buf, hs->response_bytes);

                  //if(debug >= 3) {
                  //  printf("response_string = ["); for(s=response_string;*s;s++) printf("%c", isprint(*s) || *s == '\n' ? *s: '_'); printf("]\n");
                  //}

                  /* Look for the beginning of an HTTP server response */
                  if( regexec(&response_preg, response_string, 3+1, response_pmatch, 0) == 0 ) {

                      if(hs->num_requests > 0 && hs->num_responses < hs->num_requests) {
                            ++total_responses;
                            --active_requests;
                          
                            ++hs->num_responses;

                            hr = hs->requests[hs->num_responses-1];
                            gettimeofday(&hr->first_byte, NULL);

                            response_code_string = strndup(response_string+response_pmatch[1].rm_so, response_pmatch[1].rm_eo-response_pmatch[1].rm_so);
                            hr->response_message = strndup(response_string+response_pmatch[2].rm_so, response_pmatch[2].rm_eo-response_pmatch[2].rm_so);
                            hr->response_header  = strndup(response_string+response_pmatch[3].rm_so, response_pmatch[3].rm_eo-response_pmatch[3].rm_so);
                            hr->response_code = atoi(response_code_string);

                            if( regexec(&content_length_preg, hr->response_header, 1+1, content_length_pmatch, 0) == 0 ) {
                                content_length_string = strndup(hr->response_header+content_length_pmatch[1].rm_so, content_length_pmatch[1].rm_eo-content_length_pmatch[1].rm_so);
                                hr->content_length = hs->content_length = atol(content_length_string);
                            } else {
                                /* "Content-Length:" header not found */
                                if(debug >= 3) debuglog("{content-length header not found in response header}");
                                hs->content_length = -1;
                            }




                      } else {
                        /*
                           Sometimes a response comes in, but we never tracked it's associated request.
                           THIS can happen legitimately right after startup, if we spy a response for a request that went through before startup,
                           so just ignore it.
                        */
                        if(debug >= 1) debuglog("UNASSOCIATED RESPONSE (%u.%u.%u.%u:%d => %u.%u.%u.%u:%d #%d)",
                            ts->addr.saddr & 0xFF, ts->addr.saddr >> 8 & 0xFF, ts->addr.saddr >>16 & 0xFF, ts->addr.saddr>>24, ts->addr.source,
                            ts->addr.daddr & 0xFF, ts->addr.daddr >> 8 & 0xFF, ts->addr.daddr >>16 & 0xFF, ts->addr.daddr>>24, ts->addr.dest,
                            hs->num_responses);
                      }
                      
                      free(response_code_string);

                      hs->response_bytes -= response_pmatch[0].rm_eo;

                      if(hs->response_bytes > 0) {
                         memmove(hs->response_buf, hs->response_buf + response_pmatch[0].rm_eo, hs->response_bytes);
                         if(debug >= 2) debuglog("{regex match length = %lld, remaining request_bytes = %lu}",
                           response_pmatch[0].rm_eo, hs->response_bytes);
                      } else keep_scanning = 0; /* no data left */

                      /* if this is a code 200, let's set in_content=1 and count content_received */
                      if(hs->content_length > 0) {
                          hs->in_content = 1;
                          if(debug >= 3) debuglog("{This was a HTTP server response with content. There are %lu bytes of content in this packet, out of a total expected %lu bytes.}",
                            hs->response_bytes, hs->content_length);
                      }
                  }  else keep_scanning = 0; /* didn't find an http response */

                  free(response_string);
            } else {
                /* we're inside the content data, we want to discard the right amount to skip the content */
                if(debug >= 3) debuglog("{This packet is inside the content from an HTTP server response. We have %lu bytes in this packet, prior to which we had received %lu bytes of content out of an expected %lu bytes total.}",
                  hs->response_bytes, hs->content_received, hs->content_length);
               
                /* if there's more data in the packet than purely content-data */
                if(hs->response_bytes > (hs->content_length - hs->content_received) ) {
                  hs->response_bytes = hs->content_length - hs->content_received;
                  hs->content_received = hs->content_length;
                } else {
                  hs->content_received += hs->response_bytes;
                  hs->response_bytes = 0;
                  //printf("{INSIDE: %ld}\n", hs->response_bytes);
                }
                if(debug >= 3) debuglog("{Now there are %lu bytes left over, and %lu bytes of content have been received out of an expected %ld.}",
                  hs->response_bytes, hs->content_received, hs->content_length);
                if(hs->content_received == hs->content_length) {
                      /*
                        * THIS MIGHT BE THE BEST PLACE TO PROCESS A COMPLETED REQUEST,
                          RATHER THAN FINISH-UP WITH IT AS SOON AS THE SERVER HEADER ARRIVES-
                          FOR ONE THING, IT WOULD ALLOW US TO FILTER ON RESPONSE CONTENT,
                          AND FILTER ON/COLLECT TOTAL-TRANSFER TIME ("LAST BYTE").
                          (allow for a "slow queries" filter).
                      */
                      hr = hs->requests[hs->num_responses-1];
                      gettimeofday(&hr->last_byte, NULL);
                
                      if(debug >= 3) debuglog("{Collected all %ld bytes of content in server response (%u.%u.%u.%u:%d => %u.%u.%u.%u:%d #%d).}",
                            hs->content_length,
                            ts->addr.saddr & 0xFF, ts->addr.saddr >> 8 & 0xFF, ts->addr.saddr >>16 & 0xFF, ts->addr.saddr>>24, ts->addr.source,
                            ts->addr.daddr & 0xFF, ts->addr.daddr >> 8 & 0xFF, ts->addr.daddr >>16 & 0xFF, ts->addr.daddr>>24, ts->addr.dest,
                            hs->num_responses
                      );
                      
                      process_http_transaction(hr);

                      
                      hs->content_length = -1;
                      hs->content_received = 0;
                      hs->in_content = 0;
                }
                if(hs->response_bytes > 0) keep_scanning = 1; else keep_scanning = 0;
            }
        }
}

void process_http_transaction(struct http_transaction *http) {
    int i, m;
    int metric_matches = 0;
    struct timeval first_byte_elapsed, last_byte_elapsed;

    timeval_subtract(&first_byte_elapsed, &http->first_byte, &http->request_start);
    timeval_subtract(&last_byte_elapsed,  &http->last_byte,  &http->request_start);

    if(debug >= 1) debuglog("\nHTTP TRANSACTION (%lu processed, %lu active sessions, %lu active requests) (%u.%u.%u.%u:%d => %u.%u.%u.%u:%d #%d)\n"
      "  RESPONSE CODE:         %d (\"%s\"),\n"
      "  REQUEST SENT AT:       %ld.%06d\n"
      "  FIRST BYTE ARRIVED AT: %ld.%06d (%ld.%06d secs AFTER REQUEST)\n"
      "  LAST BYTE ARRIVED AT:  %ld.%06d (%ld.%06d secs AFTER REQUEST)\n"
      "  METHOD:                %s\n"
      "  CONTENT-LENGTH:        %ld\n"
      "  URL: %s",
      total_responses, active_sessions, active_requests,
      http->addr.saddr & 0xFF, http->addr.saddr >> 8 & 0xFF, http->addr.saddr >>16 & 0xFF, http->addr.saddr>>24, http->addr.source,
      http->addr.daddr & 0xFF, http->addr.daddr >> 8 & 0xFF, http->addr.daddr >>16 & 0xFF, http->addr.daddr>>24, http->addr.dest,
      http->connection_index+1,

      http->response_code, http->response_message,
      
      http->request_start.tv_sec, http->request_start.tv_usec,
      http->first_byte.tv_sec, http->first_byte.tv_usec, first_byte_elapsed.tv_sec, first_byte_elapsed.tv_usec,
      http->last_byte.tv_sec, http->last_byte.tv_usec,   last_byte_elapsed.tv_sec,  last_byte_elapsed.tv_usec,
      
      http->method, http->content_length, http->url
    );
    if(debug >= 2) debuglog("\n  REQUEST HEADERS: \"%s\"", http->request_header);
    if(debug >= 2) debuglog("\n  RESPONSE HEADERS: \"%s\"", http->response_header);

    /* watch for header "Content-Encoding: gzip" and gunzip the data */

    /* check for metrics match */
    for(i = 0 ; i < num_metrics ; i++) {
        if(debug >= 3) debuglog("  Checking if HTTP request matches metric #%d: \"%s\"", 
          i+1, metric[i]->name);

        metric_matches = 1;
        for(m=0;m<metric[i]->num_request_header_patterns;m++) {
          if(debug >= 2) debuglog("[metric %d, req.header pattern %d]: Searching request header for \"%s\" matching \"%s\".",
            i, m,
            metric[i]->request_header_patterns[m]->header_name,
            metric[i]->request_header_patterns[m]->header_value_regex);
          if( regexec(&metric[i]->request_header_patterns[m]->header_value_preg, http->request_header, 0, NULL, 0) == 0 ) {
            if(debug >= 3) debuglog("  Request header \"%s\" matches regex: \"%s\".",
              metric[i]->request_header_patterns[m]->header_name,
              metric[i]->request_header_patterns[m]->header_value_regex);
          } else metric_matches = 0;
        }

        for(m=0;m<metric[i]->num_response_header_patterns;m++) {
          if(debug >= 2) debuglog("[metric %d, rsp.header pattern %d]: Searching response header for \"%s\" matching \"%s\".",
            i, m,
            metric[i]->response_header_patterns[m]->header_name,
            metric[i]->response_header_patterns[m]->header_value_regex);
          if( regexec(&metric[i]->response_header_patterns[m]->header_value_preg, http->response_header, 0, NULL, 0) == 0 ) {
            if(debug >= 3) debuglog("  Response header \"%s\" matches regex: \"%s\".",
              metric[i]->response_header_patterns[m]->header_name,
              metric[i]->response_header_patterns[m]->header_value_regex);
          } else metric_matches = 0;
        }

        if(metric[i]->method_regex) {
          if( regexec(&metric[i]->preg_method_regex, http->method, 0, NULL, 0) == 0 ) {
            if(debug >= 3) debuglog("  Method regex matches: \"%s\".", metric[i]->method_regex);
          } else metric_matches = 0;
        }
        if(metric[i]->url_regex) {
          if( regexec(&metric[i]->preg_url_regex,    http->url,    0, NULL, 0) == 0 ) {
            if(debug >= 3) debuglog("  URL regex matches: \"%s\".", metric[i]->url_regex);
          } else metric_matches = 0;
        }

        if(metric_matches) {
            if(debug >= 1) debuglog("  HTTP request matches metric #%d: \"%s\".", i, metric[i]->name);
          
            ++metric[i]->total_count;
            ++metric[i]->count_since_last_interval;
            metric[i]->total_first_byte += first_byte_elapsed.tv_sec * 1000000.0 + first_byte_elapsed.tv_usec;
            metric[i]->value_since_last_interval += first_byte_elapsed.tv_sec * 1000000.0 + first_byte_elapsed.tv_usec;
            if(debug) debuglog("  `%s'[total_count] = %lu", metric[i]->name, metric[i]->total_count);
        }
    } /* for() */
}
