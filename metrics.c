/*
    metrics.c
    FIX: setup mutex's for the metric counters, so that a race-condition won't un-reset a counter that
    was just reset at the interval.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#include <time.h>

#include "metrics.h"
#include "processor.h"

char *graphite_server = NULL;
int graphite_port = -1;

struct metric_struct **metric = NULL;
int num_metrics = 0;

char *metric_prefix = "";

/* local prototypes */
int send_to_graphite(char *metric_name, float value, time_t unixtime);
void report_urlmetrics_stats();

struct metric_struct *new_metric(char *name, unsigned int interval, char *method_regex, char *url_regex) {
    struct metric_struct *m = NULL;
    
    m = malloc(sizeof(struct metric_struct));

    if(method_regex) {
        if( regcomp(&m->preg_method_regex, method_regex, REG_EXTENDED|REG_ENHANCED) != 0 ) {
            fprintf(stderr, "Method regex \"%s\" for metric \"%s\" failed compilation!\n", method_regex, name);
            exit(1);
        }
    }
    if(url_regex) {
        if( regcomp(&m->preg_url_regex, url_regex, REG_EXTENDED|REG_ENHANCED) != 0 ) {
            fprintf(stderr, "URL regex \"%s\" for metric \"%s\" failed compilation!\n", url_regex, name);
            exit(1);
        }
    }

    m->name = strdup(name);
    m->interval = interval;
    m->method_regex = method_regex ? strdup(method_regex) : NULL;
    m->url_regex = url_regex ? strdup(url_regex) : NULL;
    m->total_count = 0;
    m->count_since_last_interval = 0;
    m->total_first_byte = 0.0;
    m->value_since_last_interval = 0.0;
    m->num_request_header_patterns = 0;
    m->num_response_header_patterns = 0;
    m->request_header_patterns = NULL;
    m->response_header_patterns = NULL;
    
    ++num_metrics;
    if(!metric) metric = malloc(sizeof(struct metric_struct *));
    else metric = realloc(metric, num_metrics * sizeof(struct metric_struct *));

    metric[num_metrics-1] = m;
    
    return m;
}

void *send_metrics(void *unused) {
    int i;
    time_t now;
    
    while(1) {
      //printf("\nSending %d metrics:\n", num_metrics);
      for(i=0;i<num_metrics;i++) {
          time(&now);
          
          //printf("Sending metric %d: %s: %lu / %.2f\n",
          //  i, metric[i]->name, metric[i]->count_since_last_interval, metric[i]->value_since_last_interval);

          send_to_graphite(metric[i]->name, 1.0 * metric[i]->count_since_last_interval, now);
    
          metric[i]->count_since_last_interval = 0;
          metric[i]->value_since_last_interval = 0.0;
      }
      
      report_urlmetrics_stats();
      
      sleep(60);
    }
    
    return NULL;
}


#define BUFSIZE 1024

int send_to_graphite(char *metric_name, float value, time_t unixtime) {
    int sockfd, n;
    struct sockaddr_in serveraddr;
    struct hostent *server;

    char buf[BUFSIZE];

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(graphite_server);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", graphite_server);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
	  (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(graphite_port);

    /* connect: create a connection with the server */
    if (connect(sockfd, &serveraddr, sizeof(serveraddr)) < 0) {
      perror("ERROR connecting to graphite server");
    }

    sprintf(buf, "%s.%s %.3f %ld\n", metric_prefix, metric_name, value, unixtime);

    /* send the message line to the server */
    n = write(sockfd, buf, strlen(buf));
    if (n < 0) {
      perror("ERROR writing to socket");
    }

    /* print the server's reply */
    //bzero(buf, BUFSIZE);
    //n = read(sockfd, buf, BUFSIZE);
    //if (n < 0) 
    //  error("ERROR reading from socket");
    //printf("Echo from server: %s", buf);
    close(sockfd);
    
    return 0;
}

void report_urlmetrics_stats() {
   time_t now;

   time(&now);

   send_to_graphite("urlmetrics.active_sessions", active_sessions, now);
   send_to_graphite("urlmetrics.active_requests", active_requests, now);
}
