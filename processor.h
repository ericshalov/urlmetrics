/*
    processor.h
*/
extern unsigned long active_sessions, active_requests;

void *tcp_processor(struct tcp_stream *ts, void **param);
void init_tcp_processor();
