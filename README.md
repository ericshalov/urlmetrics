urlmetrics
==========
Server daemon to sniff an interface for HTTP traffic and look for specified
patterns, reporting matches ("hits") and server response time for matching
requests to graphite (carbon).

Configuration
-------------
urlmetrics is configured in urlmetrics.conf.

Dependencies
------------
Requires flex (>= 2.5.35), bison (>= 2.3), libnids (>= 1.24)

PCRE2 (>= 10.20):
ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre2-10.20.tar.gz

License:
--------
urlmetrics is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
