%{
#include <stdio.h>

#include "config.tab.h" // to get the token types that we return

int linenum = 1, colnum = 1;

%}

/* Not needed */
%option noinput
%option nounput

WhiteSpace	[\r\t\f\v ]+

Input		"input"
Interface	"interface"

Graphite	"graphite"
Metric_Prefix	"metric_prefix"
LogLevel	"loglevel"
Metric		"metric"
Method		"method"
Request_Header	"request_header"
Response_Header	"response_header"
URL		"url"
Interval	"interval"
Server		"server"
Port		"port"

STRING		[^\"]*

NewLine	\n
START_OF_BLOCK	1
END_OF_BLOCK	2
SEMICOLON	";"
INTEGER		[0-9]+
FLOAT		[0-9]+\.[0-9]+

%x comment
%x string

%%
<INITIAL,comment,string>{
    [^\n]		{ ++colnum; } REJECT;
    \n			{ ++linenum; colnum = 1; } REJECT;  // increment line count, but don't return a token
}

<INITIAL>\"		{ BEGIN string; }

<string>{
    \"			{ BEGIN INITIAL; }
    {STRING}		{ yylval.sval = strdup(yytext); return STRING; }
}

<INITIAL>{
    "#"			BEGIN comment;
    {WhiteSpace}	
    "{"			{ return START_OF_BLOCK; }
    "}"			{ return END_OF_BLOCK; }
    {SEMICOLON}		{ return SEMICOLON; }

    {Input}		{ return Input; }
    {Interface} 	{ return Interface; }
    
    {Graphite}		{ return Graphite; }
    {Server} 		{ return Server; }
    {Port}		{ return Port; }
    
    {Metric_Prefix}	{ return Metric_Prefix; }
    {LogLevel}		{ return LogLevel; }

    {Metric}		{ return Metric; }
    {Method}		{ return Method; }
    {URL} 		{ return URL; }
    {Request_Header}	{ return Request_Header; }
    {Response_Header}	{ return Response_Header; }
    {Interval}		{ return Interval; }
    
    {FLOAT}		{ yylval.fval = atof(yytext);   return FLOAT; }
    {INTEGER}		{ yylval.ival = atoi(yytext);   return INTEGER; }
    [ \t\n]		;
}

<comment>{
    \n			BEGIN INITIAL;
    [^\n]*
}
%%
