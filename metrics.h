/*
    metrics.h
*/
#include <time.h>
/*#include <pcre2posix.h>
#define REG_DOTALL 0
#define REG_UNGREEDY REG_ENHANCED*/
#include <regex.h>

struct header_pattern {
    char *header_name;
    char *header_value_regex;
    regex_t header_value_preg;
};

struct metric_struct {
  char *name;
  char *method_regex;
  char *url_regex;
/*
  char *client_header_regex;
  char *response_code_regex;
  char *content_length_regex;
  char *response_text_regex;
  add seq number to metric
  exclusion regexes
*/
  unsigned int interval; /* seconds */
  
  regex_t preg_method_regex, preg_url_regex;
  
  unsigned long total_count;
  unsigned long count_since_last_interval;
  double total_first_byte;
  double value_since_last_interval;
  
  int num_request_header_patterns;
  struct header_pattern **request_header_patterns;
  int num_response_header_patterns;
  struct header_pattern **response_header_patterns;
};

extern struct metric_struct **metric;
extern int num_metrics;
extern char *metric_prefix;
extern char *graphite_server;
extern int graphite_port;

/* prototypes */
struct metric_struct *new_metric(char *name, unsigned int interval, char *method_regex, char *url_regex);
void *send_metrics(void *unused);
int send_to_graphite(char *metric_name, float value, time_t unixtime);
