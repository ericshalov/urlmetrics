/*

  urlmetrics.c
  (C)Copyright 2015 Eric Shalov.
  
  Report matches ("hits") and server response time for matching requests to
  graphite (carbon).
  
*/

#include <stdio.h>
#include <nids.h>
#include <ctype.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <pthread.h>
#include <unistd.h>
#include <stdarg.h>

#include "config.h"
#include "metrics.h"
#include "processor.h"
#include "urlmetrics.h"

int debug = 0;

FILE *logfile = NULL;

char *log_filename = "/tmp/urlmetrics.log";

/* prototypes */
int timeval_subtract (struct timeval *result,struct timeval *x,struct timeval *y);

int main(int argc, char *argv[]) {
    pthread_t send_metrics_thread;
    pid_t pid;

    if(log_filename) {
        if( ! (logfile = fopen(log_filename,"a")) ) {
            fprintf(stderr,"Unable to open debug log file `%s'.\n", log_filename);
            exit(1);
        }
    }

    load_config("urlmetrics.conf");

    if( geteuid() != 0 ) {
       fprintf(stderr,"%s must be run as root.\n", argv[0]);
       exit(1);
    }

    if(interface) nids_params.device = interface;
    nids_params.pcap_filter = NULL;
    nids_params.pcap_timeout = 1024; /* skip out every second to reset interval counters */

    nids_init();

    init_tcp_processor();
    nids_register_tcp(&tcp_processor);

    if( (pid = fork()) != 0) {
         /* parent */
         printf("urlmetrics started in the background with pid %d. Debug level is %d.\n", pid, debug);
         debuglog("urlmetrics started in the background with pid %d.", pid);
         exit(0);
    } else {
         chdir("/"); /* prevent process from keeping a filesystem from unmounting, depending on it's cwd at start */
         setsid();
         setpgid(0, 0); /* detach from controlling terminal */
         close(STDIN_FILENO);
         close(STDOUT_FILENO);
         close(STDERR_FILENO);
    }

    if(pthread_create(&send_metrics_thread, NULL, send_metrics, NULL)) {
        fprintf(stderr, "Error creating thread for send_metrics.\n");
        return 1;
    }


    printf("Sniffing...\n");    

    while(1) {
        //printf("nids_dispatch() ...\n");
        //nids_dispatch(1);
        nids_next();
        //printf("."); fflush(stdout);
    }
    //nids_run();

    fclose(logfile);
    
    return 0;
}

void debuglog(char *fmt, ...) {
    va_list ap;

    if(logfile) {
        va_start(ap, fmt);
        vfprintf(logfile, fmt, ap);
        va_end(ap);
        fprintf(logfile, "\n");
    }
}
