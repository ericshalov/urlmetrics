/*
    config.h
*/
extern char *interface;
extern char *graphite_server;
extern int graphite_port;
extern char *metric_prefix;

void load_config(char *filename);
